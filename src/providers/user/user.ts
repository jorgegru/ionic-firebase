import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFire, FirebaseListObservable } from "angularfire2";
import { User } from "../../models/user.model";
import { Observable } from "rxjs";
import { BaseService } from "../base.service";

@Injectable()
export class UserProvider extends BaseService {

  users: FirebaseListObservable<User[]>;

  constructor(
    public af: AngularFire,
    public http: Http
  ) {
    super();
    this.users = this.af.database.list(`/users`);
  }

  create(user: User): firebase.Promise<void>{
    return this.af.database.object(`/users/${user.uid}`)
    .set(user)
    .catch(this.handlePromiseError);
  }

  userExtists(username: string): Observable<boolean>{

    return this.af.database.list(`/users`, {
      query: {
        orderByChild: 'username',
        equalTo: username
      }
    }).map((users: User[]) => {
      return users.length > 0;
    }).catch(this.handleObservableError);

   
  }

}
