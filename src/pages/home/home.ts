import { UserProvider } from './../../providers/user/user';
import { User } from './../../models/user.model';
import { FirebaseListObservable } from 'angularfire2';
import { SignupPage } from './../signup/signup';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  users: FirebaseListObservable<User[]>;

  constructor(
    public navCtrl: NavController,
    public userService: UserProvider
  ) {

  }

  ionViewDidLoad(){
    this.users =this.userService.users;
  }

  onChatCreate(user: User): void{
    console.log('User', user);

  }

  onSignup(){
    this.navCtrl.push(SignupPage);
  }  
}
