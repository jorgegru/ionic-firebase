import { FirebaseAuthState } from 'angularfire2';

import { AuthProvider } from './../../providers/auth/auth';
import { UserProvider } from './../../providers/user/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { BaseService } from "../../providers/base.service";



@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage extends BaseService {

  signupForm: FormGroup;

  constructor(
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public authService: AuthProvider,
    public userServise: UserProvider,
    public loadingCtrl: LoadingController
  ) {
      super();
      let emailRegex = '.{6,}'

      this.signupForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(3)]],
        username: ['', [Validators.required, Validators.minLength(3)]],
        email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
        password: ['', [Validators.required, Validators.minLength(6)]],
        
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  onSubmit(){

    let loading: Loading = this.showLoading();
    let formUser = this.signupForm.value;
    let username: string = formUser.username;

    this.userServise.userExtists(username)
      .first()
      .subscribe((userExists: boolean) => {
        if(!userExists){

          this.authService.createAuthUser({
            email: formUser.email,
            password: formUser.password
          }).then((authState: FirebaseAuthState) => {
            delete formUser.password;
            formUser.uid=authState.auth.uid;
      
            this.userServise.create(formUser)
            .then(() => {
              console.log('Usuario Cadastrado');
              loading.dismiss();
            }).catch((error: any) => {
              console.log(error);
              loading.dismiss();
              this.showAlert(error);
            });
      
          }).catch((error: any) => {
            console.log(error);
            loading.dismiss();
            this.showAlert(error)
          });

        }else{
          this.showAlert(`O username ${username} já está sendo usado em outra conta!`);
          loading.dismiss();
        }
      });
  }


  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    loading.present();
    return loading;
  }

  private showAlert(message: string): void{
    this.alertCtrl.create({
      message: message,
      buttons:['OK']
    }).present();
  }

}
