import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignupPage } from './../pages/signup/signup';
import { HttpModule } from '@angular/http';

import { AngularFireModule, FirebaseAppConfig } from 'angularfire2';
import { UserProvider } from '../providers/user/user';
import { AuthProvider } from '../providers/auth/auth';

const firebaseAppConfig: FirebaseAppConfig = {
  apiKey: "AIzaSyBCig0qfBMUCl0kyRyMyGeBCXT-dUc_060",
  authDomain: "ionic-firebase-b3d99.firebaseapp.com",
  databaseURL: "https://ionic-firebase-b3d99.firebaseio.com",
  storageBucket: "ionic-firebase-b3d99.appspot.com",
  messagingSenderId: "499471765777"
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAppConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    AuthProvider
  ]
})
export class AppModule {}
